# hands-on-part2

NoF Hands-on dealing with ONAP.

It is assumed that you have already some notions on:

- [ ] system commands
- [ ] git
- [ ] docker
- [ ] orchestration

If not you can start with the [hands-on-part1](https://gitlab.forge.orange-labs.fr/lucy/sandbox/tp-nof/hands-on-part1)

At the end of this hands-on, you shall be more familiar with the following
features.

- [ ] ONAP overview
- [ ] Orange Openlab Connection
- [ ] ONAP VNF packaging
- [ ] ONAP SDC (Service modelization)
- [ ] Onboard a VNFs manually
- [ ] Instantiate a VNF with Newportal
- [ ] Main APIs (AAI, SO, SDC, SDNC, APPC)

For windows users, we propose a Linux VM available with a lot of pre-defined things [Linux_VM](http://sinawiki.rd.francetelecom.fr/data/Linux_VM_training_Onap.zip)

To run that VM you need to install&run [VMware player](https://my.vmware.com/fr/web/vmware/free#desktop_end_user_computing/vmware_workstation_player/15_0)

That VM is build based on that [VM](https://gitlab.forge.orange-labs.fr/dsif-api-tools/vm-linux-dev/blob/master/README.md)


## ONAP overview

A complete high level free [ONAP MooC](https://www.edx.org/course/introduction-to-onap)
has been created by the Linux Foundation.
It is recommended to follow this MooC to have a good high level view.

A book helps to discover ONAP:[ONAP Demystified](https://plazza.orange.com/docs/DOC-863157?sr=search&searchId=5846df7d-3298-4070-b64b-89f0fa377a5d&searchIndex=0)

An official documentation is available at https://docs.onap.org/en/casablanca/
as well as a [wikipedia](https://en.wikipedia.org/wiki/ONAP) page.

[![alt text](./img/onap-video.png)](https://www.youtube.com/watch?v=xFupe2g5S1U "Introduction to ONAP")

Orange e-learning trainings are also available:  
* [SDN/NFV and Orchestration backgrounder](https://idpfds.sso.infra.ftgroup/affwebservices/public/saml2sso?SPID=ole-01&ProtocolBinding=urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST&RelayState=/LMS/LoDetails/DetailsLo.aspx?loid=%20c79b7abf-9811-4f51-8a67-4e57ed113f3d)
* [ONAP Principles and Architecture](https://idpfds.sso.infra.ftgroup/affwebservices/public/saml2sso?SPID=ole-01&ProtocolBinding=urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST&RelayState=/LMS/LoDetails/DetailsLo.aspx?loid=%20cede3ce6-8a4d-40cc-bdb5-1b9889b689c1)


ONAP means **Open Networking Automation Platform**.

* An Open Source project managed by the Linux foundation involving lots of vendors & service providers
* Initially based on the merge of Ecomp (ATT/Amdocs) and OpenO (Huawei/China Mobile)
* First release on 2017, a release every 6 months (Amsterdam > Beijing > Casablanca > Dublin > ...)

To summarize, at a very high level, it is a big toolbox aiming to automate,
monitor and manage Virtual Network Functions (VNFs).

The ONAP Dublin architecture can be described as follows:

![ONAP architecture](./img/ONAP-toplevel_dublin.png)

It can be split into 2 main systems:

* A design system
* A Runtime system

### The Design system

This system is used to design a service. The main component is called SDC.
Additional Design components may be also used such as CLAMP for the closed loop
design.

### The Runtime system

The Runtime system includes lots of components:

* An Inventory (Database - the Datamodel herited from AT&T OpenEcomp Data model)
* A Business Process Management and Notation ([BPMN](https://en.wikipedia.org/wiki/Business_Process_Model_and_Notation))
* A Network manager (SDN controler) to manage network equipments
* An event manager and collector
* Analytics capabilities including policy manager to perform dynamic scaling
* A Bus to allow realtime communication between the different components
* and many more...

## Orange Openlab Connection

In order to play with ONAP, we need to connect to an ONAP instance: the Orange
Openlab.

#### Connect to the jumphost through SSH.

For the hands-on a user `training_user` has been automatically created.
It triggers the creation of unix account on the jumphost and and OpenStack user.
From the Jumphost you will be able to execute OpenStack commands.

The Openlab is reachable on Internet through this SSH connection.

You need to be declared in the [openlab-access](https://gitlab.forge.orange-labs.fr/onap/openlab-access)
project. The procedure is fully described in [ONAP wiki](https://wiki.onap.org/display/DW/Orange+OpenLab)

You must provide different information:

* Name
* Company
* Mail
* Motivation to use ONAP
* Start/End testing date
* Your public SSH Key

For the hands-on we will use the temporarily user `training_user`.

The private key corresponding to this user can be found on http://sinawiki.rd.francetelecom.fr/data/.
Get the training_user private key, put it on your .ssh home directory, set
the file right properly

```
chmod 600 training_user
```

Please note that the keys will be revoked after the hands-on.

You shall be able to connect to the jumphost.

```
$ ssh -i training_user training_user@onap.pod4.opnfv.fr -D 1080
$ ls
 openstack_openrc  README
```

#### openstack_openrc file

It corresponds to the credentials of the user `training_user`.
As any openlab member, a shared tenant is provided.

See the details by typing:

```
cat openstack_openrc
```

you can give a try to run basic OpenStack commands:

```
source openstack_openrc
openstack project list
openstack user list
openstack stack list
....
```

See [OpenStack CLI](https://docs.openstack.org/python-openstackclient/latest/cli/command-list.html)
commands for more details.

#### README

The README file on the jumphost will provide the list of the different urls,
login and passwords for the different components.

It also explains how to access the different portals from your web browser.

```
cat README
```

#### First connection to the portals

if you try to type directly the address of the portal http://portal.api.simpledemo.onap.org:30215/ONAPPORTAL/login.htm, it will not
work.

You need to use a **SOCKS proxy**, see the details in the README.

Once you properly set this proxy you should have access to the ONAP portal and
the Openstack Horizon portal.

Check that both portals are responding.

![Portal ONAP](./img/portal-onap.png) ![Portal OpenStack](./img/portal-openstack.png)

## ONAP VNF packaging

The way VNFs are packaged and provided to infrastructure is still a hot
topic. ONAP supports [TOSCA](https://en.wikipedia.org/wiki/OASIS_TOSCA) and [Heat](http://superuser.openstack.org/articles/quick-intro-openstack-heat/)
format.

AT&T contributed a Heat template validator (Ice validator) renamed [vvp](https://git.onap.org/vvp/validation-scripts/) to force some syntax
harmonization accross the VNF vendors. Heat is widely adopted, unlike TOSCA, it
is therefore very useful to adopt a common way to declare heat templates as by
default almost everything is configurable.

<!-- We created a portal to host this vvp verification.
Try it using the Heat template available under the heat_template directory
**ubuntu16** of this repository. Create a zip and test it on
[little vvp portal](http://littlevvp.opnfv.fr/vvp/)

Correct the errors you got (hint:there are 3 real errors). -->

If you want to automate heat linting, you can have a look at the [VVP](https://docs.onap.org/en/latest/submodules/vvp/documentation.git/docs/validation-scripts.html)

To check your heat template, you can type the following command:

```
docker run --rm -i -v <path of the dir containing the heat descriptors>:/template -v <path where you want to put the results>:/reports nexus3.onap.org:10001/onap/vvp/validation-scripts --template-directory=/template --output-directory=/reports
```

The results will be available in the directory you mentioned.

## Initiate a Heat stack directly from OpenStack

You can instantiate a Heat stack from the CLI on the jumphost or more easily
from OpenStack horizon.

```
Connect on the Horizon portal
Browse Project > Orchestration > Stacks (Piles)
Click on Launch a Stack
Connect on the Jumphost
source openstack_openrc
openstack stack list
```

More details on Heat stack creation using CLI in [Heat tutorial](https://docs.openstack.org/heat/queens/getting_started/create_a_stack.html).

## ONAP SDC (Service modelization)

We are now able to deploy a Heat Stack whose format is compatible with ONAP
rules, verified through the vvp tool.

It is now time to start playing with ONAP.
Let's start with the Design part called SDC.

* Connect to the [main portal](http://portal.api.simpledemo.onap.org:8989/ONAPPORTAL/login.htm) as cs0008 user
* Select the SDC menu

You should see the following screen:
![Portal SDC](./img/portal-sdc.png)

The official documentation of the SDC for the Beijing release can be found on
[SDC documentation](https://docs.onap.org/en/casablanca/submodules/sdc.git/docs/index.html)

Let's discover the different menus and play with the service composition.
From the SDC Home Roll over the  **Add** menu then select **Add Service**.
Complete the General info and click on **Create** .Then click on the
**Composition** Menu, you can drag & drop components already available in ONAP
to design your service.

![Portal SDC](./img/portal-sdc2.png)

Ideally 90% of the people dealing with ONAP should only deal with the SDC to
design services. Most of the stuff behind (SO, A&AI, APIs,...) should be
transparent.

The SDC as most of the ONAP components has been designed by AT&T. It reflects
lots of traditional service provider concerns. For instance the SDC defines
several roles:

* tester
* designer
* governor
* ops
* superuser

The call flow to onboard (then instantiate) services requires a task from each
role.

As you can see, on the ONAP Openlab, several services are already onboarded.
The promise/dream consisting in a one click deployment from a designed
service is not fully fulfilled yet.

## Instantiate a service using onap_tests python tools

[onap_tests](https://gitlab.com/Orange-OpenSource/lfn/onap/onap-tests) is a testing tool created by Orange and written in python. It is used to test ONAP components.
The main features are the onboarding and instantiation of  services.
To ease its usage, onap_kit project contains some predefined ansible roles to onboard and instantiate the following network service composed of:
* Packet generator
* VNF to test  

### Prepare environment
1. You need Ansible on your machine, running/linked with python3
2. Git clone onap_kit (https://gitlab.forge.orange-labs.fr/onap/onap_kit)
3. Update environment variables in vars/global.yml especially install_http.  

To test your Ansible installation : 
```
ansible --version | grep "python version"
```

if Ansible is not installed:
```
pip3 install ansible
```

if Ansible is not running with python3:
```
sudo apt-get autoremove ansible
pip3 install ansible
```


To test http proxy value:
```
http_proxy='http://127.0.0.1:3128' https_proxy='http://127.0.0.1:3128' curl google.com
```

3. Don't forget to open ssh tunnel with port 1080 towards Openlab
```
ssh -i training_user_rsa training_user@onap.pod4.opnfv.fr -D 1080
```

4. Copy the public ssh key training_user_rsa.pub in variable key_value in vars/service.yml (especially if you use your personal user login on openlab, done for training_user)     

5. Define your service_number in vars/service.yml
6. Update vars/user_cloud.yml if you don't use the user training_user  
Replace "training_user" by your user identifier (2 changes) and update the password based on the file openstack_openrc.  

### Onboarding in SDC Catalog and Distribution to ONAP running part  

1. Check the file roles/onboard_NOF/templates/NOFtest-service.j2 that will allow to upload values in SDNC for instantiation.
For example, the VNFs will use admin network and it shall exist.   
To check if it exists, on OpenLab:
```
source openstack_openrc  
openstack network list
```

2. Run the Onboarding from your computer VM in a new terminal
```
cd onap_kit
# This command shall be run in the directory of onboardNOF.yaml
ansible-playbook -i inventory/inventory onboardNOF.yaml
```

3. Check the execution running in a new terminal (on your computer)
```
tail -f onap_tests.debug.log
```

4. Connect to the SDC portal to see the created service  

### Instantiation of NOFtest service
1. Run the instantiation from your computer VM
```
ansible-playbook -i inventory/inventory instantiate.yaml
```   
2. Check the logs opening the computer terminal running following command
```
tail -f onap_tests.debug.log
```
3. Connect to VID portal to check the created service instance
4. Check that created VMs are running on OpenLab (from jumphost)
```
openstack server list|grep NOF
```
You can identify private IP address of each deployed VMs: NOF_vnfut and NOF_pktgen  
5. Check the ssh connection to NOF_pktgen
```
# Connect to jumphostNOF
ssh-add training_user_rsa
ssh -J training_user@onap.pod4.opnfv.fr ubuntu@10.4.2.100
# Attempt connection to NOF_pktgen from jumphostNOF
ssh -i training_user_rsa ubuntu@privateIPOfNOF_pktgen
```
